﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ancestry.Web.Models
{
    public class SearchResult
    {
        public int PageCount { get; set; }
        public int PageNumber { get; set; }
        public IEnumerable<Person> Data { get; set; }
        public ResponseState State { get; set; }
        public string Message { get; set; }
    }

    public enum ResponseState
    {
        Success = 1,
        NoRecords = 2,
        Error = 3
    }

}
