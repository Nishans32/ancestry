using Ancestry.Web.Helpers;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Ancestry.Test
{
    [TestClass]
    public class DataHelperTests
    {
        private SimpleJsonSerializer _jsonSerializer;

        [TestInitialize]
        public void Initialize()
        {
            var config = new ConfigurationBuilder().AddJsonFile("AppSettings.json").Build();
            _jsonSerializer = new SimpleJsonSerializer(config["FilePath"]);
        }

        [TestMethod]
        public void GetPeople_Success()
        {
            //Arrange
            DataHelper dataHelper = new DataHelper(_jsonSerializer);


            //Act
            var result = dataHelper.GetPeople("Shellie","M");

            //Assert
            
            //returned object is not null
            Assert.IsNotNull(result);
            
            //returned object contains data
            Assert.IsTrue(result.Count() >0);

        }

        [TestMethod]
        public void Get_Ascendents_success()
        {
            //Arrange
            DataHelper dataHelper = new DataHelper(_jsonSerializer);

            //Act
            var list = dataHelper.GetAncestors("Otha Sophey", "F");

            //Arrange
            Assert.IsNotNull(list);
            Assert.IsTrue(list.Count() > 0);
        }


        [TestMethod]
        public void Get_descendents_success()
        {
            //Arrange
            DataHelper ds = new DataHelper(_jsonSerializer);

            //Act
            var list = ds.GetDescendents("Terra Jaimie", "F");

            //Arrange
            Assert.IsNotNull(list);
            Assert.IsTrue(list.Count() > 0);
        }
    }
}
