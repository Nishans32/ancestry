using Ancestry.Web.Helpers;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ancestry.Test
{
    [TestClass]
    public class JsonDeserializerTest
    {
        private string _filePath;

        [TestInitialize]
        public void Initialize()
        {
            var config = new ConfigurationBuilder().AddJsonFile("AppSettings.json").Build();
            _filePath = config["FilePath"];
        }

        [TestMethod]
        public void Deserialize_Success()
        {
            //Arrange
            SimpleJsonSerializer simpleJsonSerializer = new SimpleJsonSerializer(_filePath);

            //Act
            var result = simpleJsonSerializer.JsonDeserialize();

            //Assert
            
            //Root object has been serialized
            Assert.IsNotNull(result);
            
            //People object has been serialized
            Assert.IsNotNull(result.People);

            //Places object has been serialized...
            Assert.IsNotNull(result.Places);
        }
    }
}
