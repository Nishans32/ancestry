﻿using Ancestry.Web.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Ancestry.Web.Helpers
{
    /// <summary>
    /// This class wraps NewtonSoft Json and uses it's capabilities to deserialise Json 
    /// </summary>
    public class SimpleJsonSerializer:IJsonSerializer
    {
        private string _JsonFilePath;

        public SimpleJsonSerializer(IConfiguration config)
        {
            _JsonFilePath = config["DataFilePath"];
        }

        public SimpleJsonSerializer(string path)
        {
            _JsonFilePath = path;
        }

        public AncestryRoot JsonDeserialize()
        {
            return JsonConvert.DeserializeObject<AncestryRoot>(File.ReadAllText(_JsonFilePath));
        }
    }

    /// <summary>
    ///   I believe that the best approach to deserializing the Json data is not using Newtonsoft json. 
    ///   Therefore I plan to implement an interface to leave provision for later changes. 
    /// </summary>
    public interface IJsonSerializer
    {
        AncestryRoot JsonDeserialize();
    }
}
