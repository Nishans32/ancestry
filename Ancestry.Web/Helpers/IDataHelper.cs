﻿using Ancestry.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ancestry.Web.Helpers
{
    public interface IDataHelper
    {
       AncestryRoot AncestryRoot { get; }
       IEnumerable<Person> GetPeople(string name, string gender);
       IEnumerable<Person> GetAncestors(string name, string gender);
       IEnumerable<Person> GetDescendents(string name, string gender);
       DataResult SearchPeople(string name, string gender, int startIndex, int recordCount);
    }
}
