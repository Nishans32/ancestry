﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ancestry.Web.Helpers;
using Ancestry.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Ancestry.Web.Controllers
{

    public class AncestryController : Controller
    {
        private IDataHelper _dataHelper;

        public AncestryController(IDataHelper dataHelper)
        {
            _dataHelper = dataHelper;
        }

        public SearchResult SearchPeople([FromBody] SearchParamDTO searchParam)
        {
            {
                try
                {
                    SearchResult searchResult = new SearchResult();

                    if (!searchParam.ValidateParams())
                    {
                        searchResult.State = ResponseState.Error;
                        searchResult.Message = "Invalid search parameters";
                        return searchResult;
                    }

                    var startIndex = searchParam.PageNumber > 0 ? searchParam.PageNumber * 10 : 0;

                    var dataResult = _dataHelper.SearchPeople(searchParam.Name, searchParam.Gender, startIndex, 10);

                    if (dataResult.Results.Count() < 1)
                    {
                        searchResult.State = ResponseState.NoRecords;
                        searchResult.Message = "No records found";
                        return searchResult;
                    }

                    searchResult.State = ResponseState.Success;
                    searchResult.PageNumber = searchParam.PageNumber;
                    searchResult.PageCount = (int)(Math.Ceiling((decimal)dataResult.RecordCount / (decimal)10));
                    searchResult.Data = dataResult.Results;

                    return searchResult;

                }
                catch (Exception e)
                {
                    return new SearchResult() { Message = "Error performing the search request", State = ResponseState.Error };
                }
            }
        }

        public SearchResult SearchAncestors([FromBody] SearchParamDTO searchParam)
        {
            try
            {
                SearchResult searchResult = new SearchResult();

                if (!searchParam.ValidateParams(true))
                {
                    searchResult.State = ResponseState.Error;
                    searchResult.Message = "Invalid search parameters";
                    return searchResult;
                }

                var people = _dataHelper.GetAncestors(searchParam.Name, searchParam.Gender);

                if (people.Count()<1)
                {
                    searchResult.State = ResponseState.NoRecords;
                    searchResult.Message = "No records found";
                    return searchResult;
                }

                searchResult.State = ResponseState.Success;
                searchResult.Data = people;

                return searchResult;
            }
            catch (Exception e)
            {
                return new SearchResult() { Message = "Error performing the search request", State = ResponseState.Error };
            }
        }

        public SearchResult SearchDesendants([FromBody] SearchParamDTO searchParam)
        {
            try
            {
                SearchResult searchResult = new SearchResult();

                if (!searchParam.ValidateParams(false))
                {
                    searchResult.State = ResponseState.Error;
                    searchResult.Message = "Invalid search parameters";
                    return searchResult;
                }

                var people = _dataHelper.GetDescendents(searchParam.Name, searchParam.Gender);

                if (people.Count() <1)
                {
                    searchResult.State = ResponseState.NoRecords;
                    searchResult.Message = "No records found";
                    return searchResult;
                }

                searchResult.State = ResponseState.Success;
                searchResult.Data = people;

                return searchResult;
            }
            catch (Exception e)
            {
                return new SearchResult() { Message = "Error performing the search request", State = ResponseState.Error };
            }
        }
    }
}