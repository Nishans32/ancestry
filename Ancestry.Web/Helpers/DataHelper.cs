﻿using Ancestry.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ancestry.Web.Helpers
{
    /// <summary>
    /// This class will preominantly help with data access
    /// </summary>
    public class DataHelper:IDataHelper
    {
        public AncestryRoot AncestryRoot { get; private set; }
        private Dictionary<int, Person> _dictPeople;
        private Dictionary<int, string> _dictPlaces;

        public DataHelper(IJsonSerializer serializer)
        {
            this.AncestryRoot = serializer.JsonDeserialize();
            _dictPeople = AncestryRoot.People.ToDictionary(a => a.Id, a => a);
            _dictPlaces = AncestryRoot.Places.ToDictionary(p => p.Id, p => p.Name);


            foreach (var person in AncestryRoot.People)
            {
                if (person.FatherId != null)
                    _dictPeople[person.FatherId.Value].Children.Add(person.Id);

                if(person.MotherId != null)
                    _dictPeople[person.MotherId.Value].Children.Add(person.Id);

                person.PlaceName = _dictPlaces[person.PlaceId];
            }

        }

        public IEnumerable<Person> GetPeople(string name, string gender)
        {
            string[] genders = gender == string.Empty ? new string[] { "M", "F" } : new string[] { gender };

            return AncestryRoot.People.Where(p => p.Name.Contains(name) && genders.Contains(p.Gender)).Take(10);
        }

        public DataResult SearchPeople(string name, string gender, int startIndex, int recordCount)
        {
            string[] genders = gender == string.Empty ? new string[] { "M", "F" } : new string[] { gender };

            var result = AncestryRoot.People.Where(p => p.Name.ToLower().Contains(name.ToLower()) && genders.Contains(p.Gender));

            DataResult dataResult = new DataResult();
            dataResult.Results = result.Skip(startIndex).Take(recordCount);
            dataResult.RecordCount = result.Count();

            return dataResult;
        }

        private IEnumerable<Person> GetAscendants(Person person, List<Person> people, string gender)
        {
            var _person = person;
            int level = 0;
            while (people.Count < 10 && _person != null)
            {
                level++;
                if (_person.MotherId != null)
                {
                    //Level from the top
                    _dictPeople[_person.MotherId.Value].TreeLevel = level;
                    people.Add(_dictPeople[_person.MotherId.Value]);
                }

                if (_person.FatherId != null)
                {
                    //Level from the top
                    _dictPeople[_person.FatherId.Value].TreeLevel = level;
                    people.Add(_dictPeople[_person.FatherId.Value]);
                }

                if (people.Count < 10)
                {
                    var parentId = gender == "M" ? _person.FatherId : _person.MotherId;
                    _person = parentId != null ? _dictPeople[parentId.Value] : null;
                }
            }
            return people;
        }

        public IEnumerable<Person> GetAncestors(string name, string gender)
        {
            var result = AncestryRoot.People
                                        .Where(p => 
                                                    p.Name.ToLower() == name.ToLower() 
                                                 && p.Gender == gender
                                               )
                                        .FirstOrDefault();

            return GetAscendants(result, new List<Person>(), gender);
        }

        public IEnumerable<Person> GetDescendents(string name, string gender)
        {
            string[] genders = gender == string.Empty ? new string[] { "M", "F" } : new string[] { gender };

            var person = AncestryRoot.People
                                            .Where(p =>
                                                          p.Name.ToLower() == name.ToLower()
                                                       && genders.Contains(p.Gender)
                                                   )
                                            .FirstOrDefault();

            if (person == null)
                return new List<Person>();

            //Maintain a Queue to prioritize the traverse order. i.e at each descendant level, we have to navigate horizontally before drilling into children 
            Queue<Person> priorityList = new Queue<Person>();
            priorityList.Enqueue(person);

            List<Person> desc = new List<Person>(GetChildren(new List<Person>(), priorityList, genders));
            
            return desc;
        }

        private IEnumerable<Person> GetChildren(List<Person> descendants, Queue<Person> queue, string[] genders)
        {
            Person currentPerson;
            IEnumerable<Person> currentSiblings;

            int treeLevel = 0;

            while (descendants.Count < 10 && queue.Count>0)
            {
                currentPerson = queue.Dequeue();

                currentSiblings = _dictPeople
                                            .Where(item => 
                                                           currentPerson.Children.Contains(item.Key)
                                                        && genders.Contains(currentPerson.Gender)
                                                    )
                                            .Select(item => item.Value)
                                            .Take(10 - descendants.Count);
                treeLevel++;
                currentSiblings.ToList().ForEach(s => { queue.Enqueue(s); s.TreeLevel = treeLevel; });

                descendants.AddRange(currentSiblings);


            }
            return descendants;
        }
    }
}
