﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ancestry.Web.Models
{
    /// <summary>
    /// These clases will mapped to the objects in the Json file
    /// </summary>
    public class AncestryRoot
    {
        [JsonProperty("places")]
        public Place[] Places { get; set; }

        [JsonProperty("people")]
        public Person[] People { get; set; }
    }

    [JsonObject("places")]
    public class Place
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class Person
    {
        public Person()
        {
            this.Children = new List<int>();
        }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("father_id")]
        public int? FatherId { get; set; }

        [JsonProperty("mother_id")]
        public int? MotherId { get; set; }

        [JsonProperty("place_id")]
        public int PlaceId { get; set; }

        public int Level { get; set; }

        public int TreeLevel { get; set; }

        //Full length gender for display purposes
        public string GenderDescription => Gender == "M" ? "Male" : "Female";

        //this will be populated with the birth place name for ease of access 
        public string PlaceName { get; set; }
        
        //This will include all children IDs.
        public List<int> Children { get; set; }
    }
}
