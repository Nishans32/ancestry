# Builder stage
FROM microsoft/aspnetcore-build:2.0.5-2.1.4 AS builder-stage

WORKDIR /app

# Copy project from the source to the docker container filesystem
COPY Ancestry.Web/*.csproj ./Ancestry.Web/

# install all dependencies
RUN dotnet restore Ancestry.Web

# Copy all remaining files to our image
COPY ./ ./
 
# Publish our application
RUN dotnet publish Ancestry.Web/Ancestry.Web.csproj -c Release -o /app/out

# Server listens on port 80
EXPOSE 80

# Runtime stage
FROM microsoft/aspnetcore:2.0 AS runtime-stage

WORKDIR /app
COPY --from=builder-stage /app/out .

#Entrypoint telling docker what file to start for our application
ENTRYPOINT [ "dotnet" ]
CMD [ "Ancestry.Web.dll" ]