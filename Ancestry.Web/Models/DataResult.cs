﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ancestry.Web.Models
{
    public class DataResult
    {
        public Person Source { get; set; }
        public IEnumerable<Person> Results { get; set; }
        public int RecordCount { get; set; }
    }
}
