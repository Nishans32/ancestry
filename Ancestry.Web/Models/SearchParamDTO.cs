﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ancestry.Web.Models
{
    /// <summary>
    /// The object that will be passed to the Web method by the client. 
    /// </summary>
    public class SearchParamDTO
    {
        public string Name { get; set; }
        public string Gender { get; set; }
        public SearchDirection Direction { get; set; }
        public int PageNumber { get; set; }

        public bool ValidateParams(bool validateGenderIsEmpty=false)
        {
            if (Name == string.Empty)
                return false;

            if (Gender != string.Empty && (Gender != "M" && Gender != "F"))
                return false;

            if (validateGenderIsEmpty && Gender == string.Empty)
                return false;

            return true;
        }
    }

    public enum SearchDirection
    {
        Ancestors = 1,
        Descendants = 2
    }
}
